package fr.ulille.iut.pizzaland;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import fr.ulille.iut.pizzaland.beans.Commande;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.CommandeDao;
import fr.ulille.iut.pizzaland.dto.CommandeCreateDto;
import fr.ulille.iut.pizzaland.dto.CommandeDto;
import fr.ulille.iut.pizzaland.dto.IngredientDto;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.Form;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

public class CommandeResourceTest extends JerseyTest{
	private CommandeDao dao;
	//private static final Logger LOGGER = Logger.getLogger(CommandeResource.class.getName());

	@Override
	protected Application configure() {
		return new ApiV1();
	}

	@Before
	public void setEnvUp() {
		dao = BDDFactory.buildDao(CommandeDao.class);
		dao.createTableAndCommandeAssociation();
	}

	@After
	public void tearEnvDown() throws Exception {
		dao.dropTableAndCommandeAssociation();
	}

	@Test
	public void testGetEmptyList() {
		Response response = target("/commandes").request().get();
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());	
		List<CommandeDto> commandes;
		commandes = response.readEntity(new GenericType<List<CommandeDto>>() {});	
		assertEquals(0, commandes.size());
	}

	@Test
	public void testGetExistingCommande() {
		Commande commande = new Commande();
		commande.setName("Tanguy");
		dao.insertWithPizzas(commande);
		Response response = target("/commandes").path(commande.getId().toString()).request(MediaType.APPLICATION_JSON).get();
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());	
		Commande result = Commande.fromDto(response.readEntity(CommandeDto.class));
		assertEquals(commande, result);	
	}

	@Test
	public void testGetNotExistingCommande() {
		Response response = target("/commandes").path(UUID.randomUUID().toString()).request().get();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(),response.getStatus());
	}

	@Test
	public void testCreateCommande() {
		Commande commande = new Commande();
		Ingredient ingredient = new Ingredient();	
		ingredient.setName("Chorizo");
		Pizza pizza = new Pizza();
		pizza.setName("Savoyarde");
		List<Pizza> pizzas = new ArrayList<>();
		pizzas.add(pizza);		
		commande.setName("Tanguy");
		commande.setPizzas(pizzas);	
		IngredientDto ingredientDto = Ingredient.toDto(ingredient);
		ingredientDto.setName("Chorizo");
		target("/ingredients").request().post(Entity.json(ingredientDto));      
		PizzaCreateDto pizzaCreateDto = Pizza.toCreateDto(pizza);
		target("/pizzas").request().post(Entity.json(pizzaCreateDto));
		CommandeCreateDto commandeCreateDto = Commande.toCreateDto(commande);
		Response response = target("/commandes").request().post(Entity.json(commandeCreateDto));
		assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
		CommandeDto returnedEntity = response.readEntity(CommandeDto.class);
		assertEquals(target("/commandes/" + returnedEntity.getId()).getUri(), response.getLocation());
		assertEquals(returnedEntity.getName(), commandeCreateDto.getName());
	}

	@Test
	public void testCreateSameCommande() {
		Commande commande = new Commande("Tanguy");
		dao.insertWithPizzas(commande);	
		CommandeCreateDto commandeCreateDto = Commande.toCreateDto(commande);
		Response response = target("/commandes").request().post(Entity.json(commandeCreateDto));		
		assertEquals(Response.Status.CONFLICT.getStatusCode(), response.getStatus());
	}

	@Test
	public void testCreateCommandeWithoutName() {
		CommandeCreateDto commandeCreateDto = new CommandeCreateDto();
		Response response = target("/commandes").request().post(Entity.json(commandeCreateDto));
		assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
	}

	@Test
	public void testDeleteExistingCommande() {
		Commande commande = new Commande();
		commande.setName("Tanguy");
		dao.insertWithPizzas(commande);
		Response response = target("/commandes/").path(commande.getId().toString()).request().delete();
		assertEquals(Response.Status.ACCEPTED.getStatusCode(), response.getStatus());	
		Commande result = dao.findById(commande.getId());
		assertEquals(result, null);
	}

	@Test
	public void testDeleteNotExistingCommande() {
		Response response = target("/commandes").path(UUID.randomUUID().toString()).request().delete();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}

	@Test
	public void testGetCommandeName() {
		Commande commande = new Commande();
		commande.setName("Tanguy");
		dao.insertWithPizzas(commande);
		Response response = target("commandes").path(commande.getId().toString()).path("name").request().get();
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		assertEquals("Tanguy", response.readEntity(String.class));
	}

	@Test
	public void testGetNotExistingCommandeName() {
		Response response = target("commandes").path(UUID.randomUUID().toString()).path("name").request().get();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}

	@Test
	public void testCreateWithForm() {
		Form form = new Form();
		form.param("name", "Tanguy");
		Entity<Form> formEntity = Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE);
		Response response = target("commandes").request().post(formEntity);
		assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
		String location = response.getHeaderString("Location");
		String id = location.substring(location.lastIndexOf('/') + 1);
		Commande result = dao.findById(UUID.fromString(id));
		assertNotNull(result);
	}
}
